defmodule Petrip do
  def example1 do
    %{
      P1 => [A],
      P2 => [B, D],
      P3 => [C, D],
      P4 => [E],
      P5 => [E],
      A => [P2, P3],
      B => [P4],
      C => [P5],
      D => [P4, P5],
      E => [P6]
    }
  end

  def preset(f, n) do
  end

  def postset(f, n) do
  end
end
