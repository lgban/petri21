defmodule Petri do
  def example1 do
    [
      [P1, A],
      [A, P2],
      [A, P3],
      [P2, B],
      [P2, D],
      [P3, C],
      [P3, D],
      [B, P4],
      [C, P5],
      [D, P4],
      [D, P5],
      [P4, E],
      [P5, E],
      [E, P6]
    ]
  end

  def preset(f, n) do
    Enum.filter(f, fn [_src, tgt] -> tgt == n end)
    |> Enum.map(fn e -> hd(e) end)
  end

  def postset(f, n) do
    Enum.filter(f, fn [src, _tgt] -> src == n end)
    |> Enum.map(fn e -> hd(tl(e)) end)
  end

end
